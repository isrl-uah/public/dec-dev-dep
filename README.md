# DECeiving DEVelopers through software DEPendencies

<!-- Presentation: https://es.slideshare.net/JavierJunqueraSnchez/the-day-i-ruled-the-world-rootedcon-2020 -->

## Project structure

- fuzzer: Name generation logic

- node: NPM package generation logic

- python: pip package generation logic

## Intro

Our target for testing the applications of the fuzzer will be the developers and system administrators, through the software dependency systems. According [Module Counts](#module_counts), the three main scripting languages repositories are npm, pypi and rubygems; for NodeJS, python and ruby languages respectively. We are going to study the process of deployment of new libraries in them, and then we are going to evalluate the [reach|scope] that it can supose in security terms.

For it we will deploy libraries with similar names (generated with the fuzzer) of the most used libraries of each dependency respository, importing the original library who they would like to install to go unnoticed (and regarding that in some cases, if we don't do it, could cause a denial of service). Unlike pypi, npm and rubygems shows some statistics of the package popularity, but its not enought for our work. For retrieving the results, we send some telemetry to our server when the package gets downloaded. It will give us:

- The number of downloads

- The profile which has downloaded: We determine if is super user or not.

- The world region using Maxmind's GeoIp database.

- The date of download.

Also, the possibility of sending this telemetry would allow us to know that, on every download, we are able to execute code remotely. During the phase of uploading "malicious" dependencies we attend to factors like who can publish or what dependency names are allowed.
