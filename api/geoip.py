import geoip2.database

reader = geoip2.database.Reader('GeoLite2-City.mmdb')

def fetch_ip(ip):
    r = reader.city(ip)
    return dict(
        country=r.country.name,
        city=r.city.name
    )

if __name__ == '__main__':
    import sys

    with open(sys.argv[1]) as f:
        conns = f.read().split('\n')

    for conn in conns:
        try:

            try:
                date,ip,is_sudo,platform, original, bad, version, *other = conn.split(',')
            except:
                date,ip,is_sudo,platform, original, bad,version = conn.split(',')

            geo = fetch_ip(ip)
            city = geo.get('city')
            country = geo.get('country')

            if other:
                other = " ".join(other)
            else:
                other = ""

            if not country:
                country = "None"
            if not city:
                city = "None"
            print("ñ".join((date,ip,is_sudo, original, bad, country, city, platform + version + other)))
        except Exception as e:
            print(e)

    reader.close()
