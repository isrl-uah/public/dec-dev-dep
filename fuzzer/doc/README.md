# Fuzzing the developer

For testing our attacks we have constructed a fuzzer that implements the different approximations we have studied. The idea is to reproduce all typical errors developers commit, and delivered errors hard to detect by they. This approximations are:

- Replace similar characters, using uriDeep's dictionary.

- Inserting different kind of spaces, in different parts of the input. Some of them are almost invisible unicode spaces.

- Making permutations over the text

- Deleting some chars

- Duplicating some characters

- Changing upper for lower case, and vice versa.

- Changing some homophonic characters

- Testing `ltr` implementations

## Homograph characters

The use of homographic attacks is widely known, but the publication of [uriDeep](https://github.com/mindcrypt/uriDeep) (a a high quality dictionary of similar characters) simplifies our work. This approach consists in change the characters of a text with similar looking characters, most of them from the unicode space. One example is with the pythons library `requests`:


| Printed    | Bytes                                  |
|------------|----------------------------------------|
| requesｔs   | `"reques\xef\xbd\x94s"`               |
| ᴦequests   | `"\xe1\xb4\xa6equests"`                |
| req𝘶ests   | `"req\xf0\x9d\x98\xb6ests"`            |

## Spaces insertion

Inserting spaces in the text can be unnoticed by the user in some cases:

- The space is at the end of the text

- The space is at first

- The space width is very short (like some unicode spaces)

With this three options we generate, for example:

| Printed   | Bytes                    |
|-----------|--------------------------|
| requests  | `"requests "`            |
| requests  | `"requests\xc2\xa0"`     |
| requests 	| `"\xef\xbb\xbfrequests"` |
| r​equests  | `"r\xe2\x80\x8bequests"` |

## Permutations

One of the most common typos is the permutation of characters when writing. In some cases, specially when the text is large, it can be imperceptible (e.g. `requests` could become `reuqests` interchanging the `q` with the next character `u`).

## Truncations

Other approach based in a common mistake is truncating words. Our code is able to delete the specified number of characters, but just deleting one has been enough in our tests:

| Truncations |
|-------------|
| rquests     |
| reuests     |
| reqests     |

## Duplicating characters

The last approximation based in typos is the duplication of characters. Due to hurry, ore sometimes lack of knowledge of english (e.g. "Is duplicating or dupplicating?""), the user repeats one character. These events could be:

| Duplications     |
|------------------|
| r**r**equests    |
| re**e**quests    |
| req**q**uests    |

## Upper/lower case [¿Cappitalize, cappitulate?]

Even though its not usually a typo (or at less, not with just one character), it can be usefull for an attacker the use of some characters changing the capital letters with lower characters, or vice versa.

##  Homophonic characters

Regarding specially in non english-speaking people, another common error is writing some characters as they sound in their native language. For example, `a` is pronounced `ei` in spanish. The fuzzer contemplates this case, and for example `requests` is pronounced `riquests`.

## RTL-LTR

This is a little bit risky approximation, because it depends of many factors and can fail in multiple parts of the chain of an attack. Is based in the behaviour of multiple systems when detects the RIGHT-TO-LEFT OVERRIDE character (U+202E). We also use LEFT-TO-RIGHT OVERRIDE character (U+202D), which could work in systems with RTL implementation.

For example, this is how two different consoles represents the string `"\xe2\x80\xae<tpircs/>(1)trela<tpircs>\xe2\x80\xad"`:

![Example with two different consoles](samples/ltr/rtl_to_ltr.png)
