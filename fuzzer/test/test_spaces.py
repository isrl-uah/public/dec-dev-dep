import fuzz
import sys

if __name__ == '__main__':
    f = fuzz.fuzz(sys.argv[1], do_similar=False, do_spaces=True, do_permutations=False,
        do_deletions=False, do_duplicates=False, do_case=False,
        do_homophonic=False, do_ltr=False)

    for replacement in f:
        try:
            print(replacement, "\t(%s)" % replacement.encode())
        except Exception as e:
            print(e)
