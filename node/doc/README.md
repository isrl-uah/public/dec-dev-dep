# npm approach

## Package generation process

npm (node package manager) is the main package manager used for Node (and also for other javascript developements). Every package has to contain this files:

- package.json

Main file where the main data of the package is defined: author, license, sub dependencies... Also, some install routines are definded. We use the preinstall script for sending the aforementioned telemetry.

```json
...  
"scripts": {
    "preinstall": "node index.js"
},
...
```

- index.js

This is the file where should be coded the entrypoint, before the installation of the package, this is the script we run. It is not mandatory to use this name.

- README.md

There sould be a description of the package. We have used different formats for each repository, but in all we have written more or less the same:

```md
# NEVER USE THIS DEPENDENCY
> It's a security test

A bad copy of {original_name} for testing homographic vulnerabilities.
```

## Package uploading

It is neccesary to create a user in [npmjs.com](npmjs.com) (our user is `homografo`), and login in the console with the client `npm`. After that, uploading the package with `npm publish` is so simple. It only accepts ASCII for naming the packages, always leaded by a letter.

## Automation

For automatize our work, we run `node/pkg_gen/gen.py`. For generating packages derivated from `bootstrap`, we should execute:

```bash
python3 node/pkg_gen/gen.py bootstrap
```
