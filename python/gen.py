import os
import shutil
import subprocess
import fuzzer.fuzz as fuzzer

def gen_setup(original_name, new_name):
        with open('template/setup.py.template') as template, open('setup.py', 'w+') as setup:
            # Two versions for version > or < specifications
            res_t = template.read().format(original_name=original_name,new_name=new_name, version="1000.0.0")
            setup.write(res_t)
            res_t = template.read().format(original_name=original_name,new_name=new_name, version="0.0.0.1")
            setup.write(res_t)

def gen_dist(original_name, new_name):
    print("Gen dist")
    shutil.copytree("base", new_name)
    gen_setup(original_name, new_name)

    subprocess.run(["python3", "setup.py", "sdist"])

    shutil.rmtree(new_name)
    os.remove('setup.py')

import sys
if __name__ == '__main__':
    for i in fuzzer.fuzz(sys.argv[1]):
      print("[*] Generation: ", i)
      gen_dist(i.original, i.result)
